package DynamickeS_Zasobnik;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author lbenko@ukf.sk
 * @see https://bitbucket.org/lbenko/fpv-ki-oop-2016-benko
 */
public class Prvok {
    
    private int hodnota;
    Prvok dalsi;
    
    public Prvok(int hodnota) {
        this.hodnota = hodnota;
        dalsi = null;
    }
    
    public int get_hodnota() {
        return hodnota;
    }
}
