package DynamickeS_Zasobnik;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author lbenko@ukf.sk
 * @see https://bitbucket.org/lbenko/fpv-ki-oop-2016-benko
 */
public class Zasobnik {
    
    Prvok vrch;        
    public Zasobnik() { vrch = null; }    
    public void Push(Prvok p) { //vloz na vrch
        if (vrch==null) {
            p.dalsi = null;
            vrch = p;
        } else {
            p.dalsi = vrch;
            vrch = p;
        }
    }    
    public Prvok Pop() {
        Prvok vysledok = vrch;
        vrch = vrch.dalsi;
        return vysledok;        
    }    
    public boolean jePrazdny() {
        if(vrch==null) return true;
        else return false;
    }
    
    public String vypis() {
        String vysledok = "";
        Prvok pom = vrch; //pomocna premenna na prechod zoznamom
        while (pom != null) {
            vysledok += pom.get_hodnota() + " \n";
            pom = pom.dalsi;
        }
        return vysledok;
    }
    
}
