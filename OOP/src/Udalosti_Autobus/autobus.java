package Udalosti_Autobus;


import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.Timer;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author lbenko@ukf.sk
 * @see https://bitbucket.org/lbenko/fpv-ki-oop-2016-benko
 */
public class autobus extends JComponent implements ActionListener{
    
    boolean motor = false;
    Timer t;
    JFrame form;
    int aktpocet = 0;
    int maxpocet = 10;
    
    public autobus(JFrame form) {
        setBounds(100,100,100,100);
        this.form = form;
        t = new Timer(10,this);
        t.setInitialDelay(0);
        t.start();
    }
    
    public void nastup() {
        if(!motor) {
            if(aktpocet<maxpocet) aktpocet++;
            repaint();
        }
    }
    
    public void vystup() {
        if(!motor) {
            if(aktpocet>0) aktpocet--;
            repaint();
        }
    }
    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.setColor(Color.BLACK);
        g.fillOval(5,25,15,15);
        g.fillOval(40,25,15,15);
        g.setColor(Color.RED);
        g.fillRect(0,0,60,30);
        g.setColor(Color.cyan);
        g.fillRect(4,4,10,10);
        g.fillRect(24,4,10,10);
        g.fillRect(44,4,10,10);
        //vykreslenie poctu cestujucich
        if(!motor) {
            //otvorene dvere = povolene nastupovanie
            g.setColor(Color.yellow);
            g.fillRect(4,4,10,20);
            //indikator zaplnenia autobusu
            g.setColor(Color.green);
            g.drawRect(0,50,maxpocet*5,10);
            g.fillRect(0, 50,aktpocet*5, 10);
        }
    }
    
    private void pohyb() {
        //setBounds(getX()+1,getY(),getWidth(),getHeight()); //ina moznost pohybu
        int x = getX();
        if(getX()<-getWidth()) x = form.getWidth();
        setLocation(x-1,getY());
        repaint();
    }
    
    public void zapni() {
        motor = !motor;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(motor) pohyb();
    }
    
}
