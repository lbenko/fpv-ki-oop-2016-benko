package Grafika_Rozne;


import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JComponent;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author lbenko@ukf.sk
 * @see https://bitbucket.org/lbenko/fpv-ki-oop-2016-benko
 */
public class pohar extends JComponent{
    
    private int maxObjem;
    private int aktObjem;
    private Color farba;
    
    public pohar() {
        maxObjem = 1000;
        farba = Color.green;
        aktObjem = 200;
        setBounds(100,100,150,150);
    }
    
    public void dolej(int kolko) {
        if(kolko+aktObjem<=maxObjem) {
            aktObjem += kolko;
            repaint();
        }
    }
    
    public void odlej(int kolko) {
        if(kolko<=aktObjem) {
            aktObjem -= kolko;
            repaint();
        }
    }
    
    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        
        g.setColor(Color.blue);
        g.fillRect(0, 0, 10, 100);
        g.fillRect(60, 0, 10, 100);
        g.fillRect(0, 100, 70, 10);
        
        int stlpec = (int)((float)aktObjem/maxObjem*100);
        System.out.println(stlpec);
        g.setColor(farba);
        g.fillRect(10,100,50,-stlpec);
        
    }
    
    
}
