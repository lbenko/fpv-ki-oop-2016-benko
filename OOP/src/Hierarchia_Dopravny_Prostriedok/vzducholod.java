package Hierarchia_Dopravny_Prostriedok;


import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JFrame;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author lbenko@ukf.sk
 * @see https://bitbucket.org/lbenko/fpv-ki-oop-2016-benko
 */
public class vzducholod extends Dopravny_Prostriedok {
        
    public vzducholod(int x, int y, JFrame rodic, double spotreba) {
        super(x, y, rodic);
        this.spotreba = spotreba; //spotreba na 100 tikov :)
        nadrz = 200;
    }
    
    @Override
    protected void vykresli(Graphics g) {
        g.setColor(Color.gray);
        g.fillOval(0, 10, 50, 15);
        g.fillRect(17, 23, 20, 6);
    }
    
    @Override
    protected void pohyb() {
        if(nadrz<=0) {
            if(getY()>=rodic.getHeight()-100) {
                t.stop();
                rodic.remove(this);
                rodic.repaint();
            } else {
                setLocation(getX(),getY()+5);
            }
        } else {
            if(getX()>rodic.getWidth()) setLocation(-getWidth(),getY());
            else {
                setLocation(getX()+3,getY());
                nadrz -= (spotreba/100);
            }
        }
    }
    
}
