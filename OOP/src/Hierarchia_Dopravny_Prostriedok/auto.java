package Hierarchia_Dopravny_Prostriedok;


import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JFrame;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author lbenko@ukf.sk
 * @see https://bitbucket.org/lbenko/fpv-ki-oop-2016-benko
 */
public class auto extends Dopravny_Prostriedok {
    
    double spotreba; //spotreba na 100 tikov :)
    
    public auto(int x, int y, JFrame rodic, double spotreba) {
        super(x, y, rodic);
        nadrz = 40.0;
        this.spotreba = spotreba;
    }
    
    @Override
    protected void vykresli(Graphics g) {
        g.setColor(Color.blue);
        g.fillRect(0, 20, 50, 10);
        g.fillOval(5, 15, 40, 10);
        g.setColor(Color.black);
        g.fillOval(5, 28, 10, 10);
        g.fillOval(35, 28, 10, 10);
    }
    
    @Override
    protected void pohyb() {
        if(nadrz<=0) {
            t.stop();
            System.out.println("Dosou benzin");
        } else {
            if(getX()<0) setLocation(rodic.getWidth(),getY());
            else {
                setLocation(getX()-3,getY());
                nadrz -= (spotreba/100);
            }
        }
    }
    
}
