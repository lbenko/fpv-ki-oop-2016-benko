package Hierarchia_Dopravny_Prostriedok;


import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.Timer;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author lbenko@ukf.sk
 * @see https://bitbucket.org/lbenko/fpv-ki-oop-2016-benko
 */
public class Dopravny_Prostriedok extends JComponent implements ActionListener {

    Timer t;
    JFrame rodic;
    double nadrz = 0;
    double spotreba = 0;
    
    public Dopravny_Prostriedok(int x, int y, JFrame rodic)  {
        setBounds(x, y, 50, 50);
        this.rodic = rodic;
        t = new Timer(50,this);
        t.start();
    }
    
    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        vykresli(g);        
    }
    
    protected void vykresli(Graphics g) {
        
    }
    
    protected void pohyb() {
        nadrz -= 1;
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        pohyb();
    }
    
}
