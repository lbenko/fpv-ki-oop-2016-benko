package Hierarchia_Balon;


import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.Timer;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author lbenko@ukf.sk
 * @see https://bitbucket.org/lbenko/fpv-ki-oop-2016-benko
 */
public class balon extends JComponent implements ActionListener, 
        MouseListener {

    Timer t;
    JFrame rodic;
    
    public balon(int x, int y, JFrame rodic) {
        setBounds(x,y,40,40);
        this.rodic = rodic;
        t = new Timer(50,this);
        t.start();
        addMouseListener(this);
    }    
    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        vykresli(g);
    }    
    @Override
    public void actionPerformed(ActionEvent e) {
        pohyb();
        repaint();
    }    
    protected void vykresli(Graphics g) {
        g.setColor(Color.yellow);
        g.fillOval(0,0,getWidth(),getHeight());
    }
    protected void pohyb() {
        if(getY()>=0) {
            setLocation(getX(),getY()-2);
        } else {
            t.stop();
            rodic.remove(this);
            rodic.repaint();
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {
    }

    @Override
    public void mousePressed(MouseEvent e) {
        kamen b = new kamen(getX()+20,getY()+getHeight(),rodic);
        rodic.add(b);
        b.repaint();
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
        
    }
    
}
