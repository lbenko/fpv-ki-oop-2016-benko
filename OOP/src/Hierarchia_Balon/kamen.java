package Hierarchia_Balon;


import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JFrame;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author lbenko@ukf.sk
 * @see https://bitbucket.org/lbenko/fpv-ki-oop-2016-benko
 */
public class kamen extends balon {
    
    public kamen(int x, int y, JFrame rodic) {
        super(x, y, rodic);
    }
    @Override
    protected void vykresli(Graphics g) {
        g.setColor(Color.black);
        g.fillOval(0,0,10,10);
    }
    @Override
    protected void pohyb() {
        if(getY()+getHeight()<=rodic.getHeight()) {
            setLocation(getX(),getY()+5);
        } else {
            t.stop();
            rodic.remove(this);
            rodic.repaint();
        }
    }
    
}
