package Udalosti_Zavora;


import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.JComponent;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author lbenko@ukf.sk
 * @see https://bitbucket.org/lbenko/fpv-ki-oop-2016-benko
 */
public class zavora extends JComponent implements KeyListener {
    
    int surX;
    int surY;
    int surX2;
    int surY2;
    
    public zavora() {
        setBounds(100,50,200,200);
        surX = 100;
        surY = 160;
        surX2 = 180;
        surY2 = surY;
        setFocusable(true);
        addKeyListener(this);
    }
    
    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.setColor(Color.black);
        g.fillRect(170, 150, 20, 50);
        vykresli(g);
    }
    
    protected void vykresli(Graphics g) {
        g.setColor(Color.red);
        g.drawLine(surX, surY, surX2, surY2);
    }
    
    protected void hore() {
        if(surX!=surX2) {
            surX++;
            surY--;
            repaint();
        }
    }
    
    protected void dolu() {
        if(surY!=surY2) {
            surX--;
            surY++;
            repaint();
        }
    }

    @Override
    public void keyTyped(KeyEvent e) {        
    }

    @Override
    public void keyPressed(KeyEvent e) {
        int k = e.getKeyCode(); // kód klávesu
        switch (k) {
            case 38: hore(); // sipka hore
            break;   
            case 40: dolu(); // sipka dolu
            break;   
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {    
    }
}
