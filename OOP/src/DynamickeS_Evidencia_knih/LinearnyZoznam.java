package DynamickeS_Evidencia_knih;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author lbenko@ukf.sk
 * @see https://bitbucket.org/lbenko/fpv-ki-oop-2016-benko
 */
public class LinearnyZoznam {
    
    Kniha zac; // ukazovatel na prvy prvok zoznamu
    Kniha kon; // ukazovatel na posledny prvok zoznamu
    
    public LinearnyZoznam() {
        zac = null;
        kon = null;
    }
    
    public void pridajKnihu(Kniha book) {
        if (zac==null) { //ak mame prazdny zoznam
            book.dalsi = null; //nema nasledovnika, nastavim null
            zac = book; //je to prvy prvok zoznamu, tak nan bude ukazovat zaciatok
            kon = zac; //koniec je aj zaciatok, cize je to ten isty prvok
        } else { //ak mame neprazdny zoznam
            book.dalsi = null; //pridavame na koniec, cize nema nasledovnika
            kon.dalsi = book; //pridam prvok za koniec
            kon = book; //koniec nastavim na tento prvok, ktory sme pridali na koniec
        }
    }
    
    public String vypisStrany(String hladana) {
        String vysledok = "";
        boolean najdene = false;
        Kniha pom = zac;
        while (pom != null) {
            if(pom.get_nazov().equals(hladana)) {
                vysledok = String.valueOf(pom.get_pocetStran());
                najdene = true;
            }
            pom = pom.dalsi;
        }
        if(!najdene) {
            vysledok = "Hľadaná kniha nebola nájdená.";
        }
        return vysledok;
    }
    
    public void odstranKnihu(String hladana) {
        Kniha pom = zac; //nastavim na zaciatok
        if(zac.get_nazov().equals(hladana)) {
            zac = zac.dalsi; //posuniem zaciatok na nasledujuci prvok
            pom = null; //uvolnim z pamati povodny zaciatok
        } else {
            Kniha pred = zac;
            pom = zac.dalsi;
            while (pom!=null) {
                if(pom.get_nazov().equals(hladana)) {
                    pred.dalsi = pom.dalsi; //nastavim prvku pred zmazanym na nasledovnika toho, co je za pom
                    pom = null;
                    return;
                }
                pom = pom.dalsi;
                pred = pred.dalsi;
            }
        }
    }
    
    public String vypis() {
        String vysledok = "";
        Kniha pom = zac; //pomocna premenna na prechod zoznamom
        while (pom != null) {
            vysledok += pom.get_nazov() + ", " + pom.get_pocetStran() + "s. \n";
            pom = pom.dalsi;
        }
        return vysledok;
    }
    
}
