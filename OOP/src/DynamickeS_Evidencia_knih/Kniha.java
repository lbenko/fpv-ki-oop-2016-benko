package DynamickeS_Evidencia_knih;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author lbenko@ukf.sk
 * @see https://bitbucket.org/lbenko/fpv-ki-oop-2016-benko
 */
public class Kniha {
    
    private String nazov;
    private int pocet_stran;
    Kniha dalsi;
    
    public Kniha(String nazov, int pocet_stran) {
        this.nazov = nazov;
        this.pocet_stran = pocet_stran;
        dalsi = null;
    }
    
    public int get_pocetStran() {
        return pocet_stran;
    }
    
    public String get_nazov() {
        return nazov;
    }
}
