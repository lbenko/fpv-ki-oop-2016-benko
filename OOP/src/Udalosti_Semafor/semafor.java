package Udalosti_Semafor;


import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JComponent;
import javax.swing.Timer;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author lbenko@ukf.sk
 * @see https://bitbucket.org/lbenko/fpv-ki-oop-2016-benko
 */
public class semafor extends JComponent implements ActionListener {
    
    boolean stav = false;
    int stav2 = -1;
    boolean zapnuty = false;
    boolean zelena = false;
    Timer t;
    
    public semafor() {
        setBounds(100,100,100,200);
        t = new Timer(1000,this);
        t.setInitialDelay(1000);
        t.start();
    }
    
    public void zapniSemafor() {
        zapnuty = true;
        zelena = true;
        t.setDelay(2000);
    }
    
    public void vypniSemafor() {
        zapnuty = false;
        zelena = false;
        stav2 = -1;
        t.setDelay(1000);
    }
    
    public void prepniSemafor() {
        zelena = !zelena;
    }
    
    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.setColor(Color.gray);
        g.fillRect(0, 0, getWidth(), getHeight());
        vykresli(g);
    }
    
    protected void vykresli(Graphics g) {
        g.setColor(Color.black);
        g.fillOval(30, 20, 40, 40);
        g.fillOval(30, 80, 40, 40);
        g.fillOval(30, 140, 40, 40);
        
        if(zapnuty) {
            vyhodnot(g);
        } else {
            if(stav) {
                g.setColor(Color.yellow);
                g.fillOval(30, 80, 40, 40);
            }
        }
    }
    
    protected void vyhodnot(Graphics g) {
        switch(stav2) {
            case 0: cervena(g);
            break;
            case 1: cervena_zlta(g);
            break;
            case 2: zelena(g);
            break;
            case 3: zlta(g);
            break;
        }
    }
    
    protected void cervena(Graphics g) {
        g.setColor(Color.red);
        g.fillOval(30, 20, 40, 40);
    }
    
    protected void cervena_zlta(Graphics g) {
        g.setColor(Color.red);
        g.fillOval(30, 20, 40, 40);
        g.setColor(Color.yellow);
        g.fillOval(30, 80, 40, 40);
    }
    
    protected void zelena(Graphics g) {
        g.setColor(Color.green);
        g.fillOval(30, 140, 40, 40);
    }
    
    protected void zlta(Graphics g) {
        g.setColor(Color.yellow);
        g.fillOval(30, 80, 40, 40);
    }
    
    protected void zmenStav() {
        stav = !stav;
        if(zapnuty) {
            if(zelena) {
                if(stav2!=2) stav2++;
            } else {
                if(stav2!=2) stav2=0;
                else stav2++;
            }
        }
        repaint();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        zmenStav();
    }
    
}
